/*
 motor.c
 *
 * Created: 18.12.2014 17:50:18
 *  Author: Rain
 */

#include "motor.h"
#include <util/delay.h>

void motor_init(void)
{
    // Set motor pins as output and set them low
    PORT_SetPinsAsOutput(&MOTOR_PORT,MOTOR_PINMASK);
    PORT_ClearPins(&MOTOR_PORT,MOTOR_PINMASK);

    // Configure PORTD Timer 0 as Single Slope PWM source 32kHz
    TC_SetPeriod(&MOTOR_TIMER,1000);
    TC0_ConfigWGM(&MOTOR_TIMER,TC_WGMODE_SS_gc);
    TC0_DisableCCChannels(&MOTOR_TIMER,(TC0_CCAEN_bm|TC0_CCBEN_bm|TC0_CCCEN_bm|TC0_CCDEN_bm));
    TC0_ConfigClockSource(&MOTOR_TIMER,TC_CLKSEL_DIV1_gc);

    TC_SetCompareA (&MOTOR_TIMER,0);
    TC_SetCompareB (&MOTOR_TIMER,0);
    TC_SetCompareC (&MOTOR_TIMER,0);
    TC_SetCompareD (&MOTOR_TIMER,0);

    PORTR.DIR = 3;
    PORTR.OUT = 0;
    _delay_ms(1);
    PORTR.OUT = 3;
}
void servo_init(void)
{
	// Set motor pins as output and set them low
	PORT_SetPinsAsOutput(&PORTC,3);
	PORT_ClearPins(&PORTC,3);
	// Configure PORTC Timer 0 as Single Slope PWM source 50Hz
	TC0_ConfigWGM(&TCC0, TC_WGMODE_SS_gc);
	TC_SetPeriod(&TCC0, 9999);
	TC0_ConfigClockSource(&TCC0, TC_CLKSEL_DIV64_gc);
	TC_SetCompareA(&TCC0, 750);       // Duty cycle
	TC0_EnableCCChannels(&TCC0, TC0_CCAEN_bm);   // Enable channel A and B
}

void speed(int16_t left)
{
    if(left > 0)
    {
        // PWM to BIN1, BIN2 low
        TC0_DisableCCChannels(&MOTOR_TIMER,TC0_CCBEN_bm);
        TC0_EnableCCChannels(&MOTOR_TIMER,TC0_CCAEN_bm);

        PORT_SetOutputBit(&MOTOR_PORT,BIN2);
        TC_SetCompareA (&MOTOR_TIMER,1001-left);
    }
    else if (left < 0)
    {
        // PWM to BIN2, BIN1 low
        TC0_DisableCCChannels(&MOTOR_TIMER,TC0_CCAEN_bm);
        TC0_EnableCCChannels(&MOTOR_TIMER,TC0_CCBEN_bm);

        PORT_SetOutputBit(&MOTOR_PORT,BIN1);
        TC_SetCompareB (&MOTOR_TIMER,1001+left);
    }
    else
    {
        // Pins LOW (coast)
        TC0_DisableCCChannels(&MOTOR_TIMER,TC0_CCAEN_bm|TC0_CCBEN_bm);
        PORT_SetPins(&MOTOR_PORT,(1<<BIN1)|(1<<BIN2));
    }
}
void steering(int8_t position)
{	
	TC_SetCompareA(&TCC0, 670-((int16_t)position*2));
}