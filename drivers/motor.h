/*
 * motor.h
 *
 * Created: 18.12.2014 17:50:18
 *  Author: Rain
 */
#ifndef MOTOR_H_
#define MOTOR_H_

#include <avr/io.h>
#include <avr/interrupt.h>
#include "board.h"
#include "drivers/tc_driver.h"
#include "drivers/qdec_driver.h"

// Motor driver PWM timer
#define MOTOR_TIMER		TCD0

// Initialize motor controller
void motor_init(void);
void servo_init(void);

void speed(int16_t);
void steering(int8_t);

#endif /* MOTOR_H_ */