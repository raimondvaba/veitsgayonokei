/*
 * veitsgayonokei.h
 *
 * Created: 02/11/2016 11:10:10
 *  Author: Raimond
 */ 


#ifndef INCFILE1_H_
#define INCFILE1_H_
#include <avr/io.h>
#define CLAMP(x, low, high)  (((x) > (high)) ? (high) : (((x) < (low)) ? (low) : (x)))
#define ABS(a)	   (((a) < 0) ? -(a) : (a))


uint16_t sharpIR[150];

uint8_t distance(uint8_t input);
uint8_t get_right_mean(uint8_t);
uint8_t get_left_mean(uint8_t);
int8_t robot_in_standstill(uint8_t left, uint8_t right);
int8_t get_sign(int32_t input);
int32_t get_steering_mean(int8_t steering);
#endif /* INCFILE1_H_ */