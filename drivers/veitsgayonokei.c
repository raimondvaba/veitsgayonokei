/*
 * veitsgayonokei.c
 *
 * Created: 02/11/2016 11:10:02
 *  Author: Raimond
 */ 
#include "veitsgayonokei.h"
#include "com.h"
#include "adc.h"

uint16_t sharpIR[150] =
{
	2040,
	2040,
	2040,
	2040,
	2040,
	2040,
	2040,
	1830,
	1655,
	1480,
	1394,
	1308,
	1222,
	1136,
	1050,
	1012,
	974,
	936,
	898,
	860,
	838,
	815,
	793,
	770,
	748,
	725,
	703,
	680,
	658,
	635,
	624,
	613,
	602,
	591,
	580,
	569,
	558,
	547,
	536,
	525,
	518,
	510,
	503,
	495,
	488,
	480,
	473,
	465,
	458,
	450,
	446,
	441,
	437,
	432,
	428,
	423,
	419,
	414,
	410,
	405,
	402,
	398,
	395,
	391,
	388,
	384,
	381,
	377,
	374,
	370,
	368,
	366,
	363,
	361,
	359,
	357,
	354,
	352,
	350,
	348,
	345,
	343,
	341,
	339,
	336,
	334,
	332,
	330,
	327,
	325,
	324,
	323,
	321,
	320,
	319,
	318,
	316,
	315,
	314,
	313,
	311,
	310,
	309,
	308,
	306,
	305,
	304,
	303,
	301,
	300,
	299,
	298,
	296,
	295,
	294,
	293,
	291,
	290,
	289,
	288,
	286,
	285,
	284,
	283,
	281,
	280,
	279,
	278,
	276,
	275,
	275,
	274,
	274,
	273,
	273,
	272,
	272,
	271,
	271,
	270,
	270,
	269,
	269,
	268,
	268,
	267,
	267,
	266,
	266,
	265,
};

uint8_t distance(uint8_t sensor)
{	
	uint16_t input = adc_read(sensor);
	for (int i=5; i<120; i++)
		if (sharpIR[i] < input) return i;
	return 120;
}
uint8_t get_left_mean(uint8_t left)
{
	static uint8_t left_stack[50] = {0};
	static uint8_t stack_count = 0;
	static uint16_t sum_left = 0;
	uint8_t meanL;
	
	sum_left += left - left_stack[stack_count];
	
	meanL = sum_left/50;
	
	left_stack[stack_count] = left;

	stack_count++;
	stack_count%= 50;

	return meanL;
}
uint8_t get_right_mean(uint8_t right)
{
	static uint8_t right_stack[50] = {0};
	static uint8_t stack_count = 0;
	static uint16_t sum_right = 0;
	uint8_t meanR;
	
	sum_right += right - right_stack[stack_count];
	meanR = sum_right/50;
	right_stack[stack_count] = right;

	stack_count++;
	stack_count%= 50;
	
	return meanR;
}
int32_t get_steering_mean(int8_t steering)
{
	static int8_t steering_stack[200] = {0};
	static int16_t stack_count = 0;
	static int32_t sum_steering = 0;
	
	sum_steering += steering - steering_stack[stack_count];
	steering_stack[stack_count] = steering;

	stack_count++;
	stack_count%= 200;
	
	return sum_steering;
}
int8_t robot_in_standstill(uint8_t left, uint8_t right)
{
	static uint16_t last_left=0;
	static uint16_t last_right=0;
	static uint8_t count = 0;
	if (left > last_left - 2 && left < last_left + 2)
		count ++;
	else
	{
		count = 0;
		last_left = left;
	}
	if (right > last_right - 2 && right < last_right + 2)
		count ++;
	else
	{
		count = 0;
		last_right = right;
	}
	if (count > 200)
	{
		count = 0;
		return 1;
	}
	else
		return 0;
}
int8_t get_sign(int32_t input)
{
	if (input > 0) return 1;
	else if (input < 0) return -1;
	else return 0;
}