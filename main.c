/*
 * main.c
 *
 * Created: 18.12.2014 17:50:18
 *  Author: Rain
	
	SEXY DOCUMENTATION, OH YEAH!
	dist(adc_read(int)) - returns IR sensor distance converted to centimeter-ish. Inputs are defined by port number (0 for left, 1 for right)
	rgb_set(int) - sets LED to a specified color RED, YELLOW, MAGENTA, BLUE, TEAL, GREEN, WHITE, PINK
	steering(int) - sets steering servo to specified position [-100;100] from left to right, scaling
	speed(int) - sets motor speed to something [-1000;1000] - reverse to forward. Recommended something from 250-300
	millis() - returns milliseconds from the board starting
	_delay_ms(int) - program does nothing for specified amounts of milliseconds. Using millis() as alternative is recommended
	_sw1_read() - returns button 1 value. 1 when pushed down, 0 when released. Same for button 2
	radio_puts(char[]) sends a char array to radio UART 
		example:
		char buff[30];
		sprintf(buff, "Left sensor %4d", dist(adc_read(1));
		radio_puts(buff);
*/

#include <avr/io.h>
#include <avr/eeprom.h>
#include "drivers/board.h"
#include "drivers/adc.h"
#include "drivers/motor.h"
#include "drivers/com.h"
#include "drivers/gyro.h"
#include "drivers/veitsgayonokei.h"
#include <avr/eeprom.h>

#define NEUTRAL_DIFFERENCE 15
#define KP 3
#define KP_D 2
#define LEFT 0
#define RIGHT 1

uint8_t distanceLeft = 0;
uint8_t distanceRight = 0;

int main(void)
{
    // Initialize peripherals
    clock_init();    // config system clock 32MHz peale
    board_init();    // config LED ja buttons
    adc_init();         // config ADC kanal 0
    radio_init(57600);  // set radio UART
    motor_init();
    servo_init();
    speed(0);
    steering(0);
	int8_t isAlive = 0;
	int8_t isStuck = 0;
	int8_t steerValue = 0;
	if(RST.STATUS & 1)
	{
		RST.STATUS = 1;
		CCPWrite(&RST.CTRL,RST_SWRST_bm);
	}
	uint32_t timeout = millis();
	int32_t probable_turning_side = 0;
    while(1)
    {	
		char radio_buffer = radio_getc_nolock();
		if (isAlive) 
		{
			if (probable_turning_side > 0)
				rgb_set(GREEN);
			else
				rgb_set(RED);
			if(!(millis()%20))
			{
				distanceLeft = get_left_mean(distance(LEFT));
				distanceRight = get_right_mean(distance(RIGHT));
				isStuck = robot_in_standstill(distanceLeft,distanceRight);
				
				steerValue = CLAMP((distanceRight - distanceLeft) * KP / KP_D, -100, 100);
				probable_turning_side = get_sign(get_steering_mean(steerValue));
				
// 				char buff[30];
// 				sprintf(buff,"%4d,%4d,%4d\n\r",distanceLeft,distanceRight,steerValue);
// 				radio_puts(buff);
			}
			if(sw2_read() || radio_buffer == 's') 
			{
				while(sw2_read());
				_delay_ms(10);
				isAlive = 0;
				speed(0);
				steering(0);
			}
			if(distanceRight < 9) 
			{
				rgb_set(BLUE);
				while(distanceRight < 13) {
					speed(-250);
					steering(100);
					distanceRight = distance(RIGHT);
				}	
			} 
			else if (distanceLeft < 9) 
			{	
				rgb_set(YELLOW);
				while(distanceLeft < 13) 
				{
					speed(-250); 
					steering(-100); 
					distanceLeft = distance(LEFT);
				}
			} 
			else if (isStuck && millis() - timeout > 2000)
			{
				timeout = millis();
				rgb_set(MAGENTA);
				speed(-250);
				steering(-100*probable_turning_side);
				_delay_ms(500);
				steering(100*probable_turning_side);
				speed(250);
				_delay_ms(500);
				isStuck = 0;
				timeout = millis();
			}
			else if (isAlive)
			{		
				speed(250);
			}
			steering(steerValue);	
		} 
		else 
		{
			rgb_set(WHITE);
			if(sw2_read() || radio_buffer == 'g') 
			{
				while(sw2_read());
				_delay_ms(10);
				isAlive = 1;
			}
			else if (radio_buffer == 'w')
			{
				speed(250);
				steering(0);
			}
			else if (radio_buffer == 'd')
			{
				steering(100);
			}
			else if (radio_buffer == 'a')
			{
				steering(-100);
			}
			else if (radio_buffer == 's')
			{
				steering(0);
				speed(0);
			}
			else if (radio_buffer == 'r')
			{
				speed(-250);
			}
		}
    }
}

// DriveTick interrupt service
ISR(DRIVE_TIMER_INTERRUPT) 
{
	
}

			
				
		
		

